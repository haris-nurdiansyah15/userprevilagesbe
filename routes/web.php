<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router)
{
    $router->post('auth/login', 'AuthController@login');
    $router->post('auth/token-refresh', 'AuthController@refresh');
    $router->get('/me', 'AuthController@me');
    $router->post('auth/logout', 'AuthController@logout');

    $router->get('user', 'UserController@index');
    $router->post('user/store', 'UserController@store');
    $router->put('user/store/{id}', 'UserController@update');
    $router->get('user/detail/{id}', 'UserController@show');
    $router->delete('user/delete/{id}', 'UserController@destroy');

    $router->get('menu', 'MenuController@index');
    $router->get('menu/parent', 'MenuController@getParentMenu');
    $router->get('menu/detail/{id}', 'MenuController@show');
    $router->post('menu/store', 'MenuController@store');
    $router->put('menu/store/{id}', 'MenuController@store');
    $router->delete('menu/delete/{id}', 'MenuController@destroy');
});
