<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username' => 'admin',
            'password' => Hash::make('Password123'),
        ]);

        $parentMenu = Menu::create(['name' => 'Management']);
        $subMenu1 = Menu::create(['name' => 'User', 'uri' => '/table-user', 'parent_id' => $parentMenu->id]);
        $subMenu2 = Menu::create(['name' => 'Menu', 'uri' => '/table-menu', 'parent_id' => $parentMenu->id]);

        $user->accesses()->attach([$subMenu1->id, $subMenu2->id]);
    }
}
