# Lumen PHP Framework

# Instalation
composer install
php artisan migrate --seed
# Generate JWT 
php artisan jwt:secret
# Run Aplication
php -S localhost:8000 -t public
