<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        try {
            $menus = Menu::with('parentMenu')->get();
            return $this->success('success retrieving data', 200, $menus);
        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }

    public function store(Request $request, $id = null)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return $this->failure($validator->errors()->first(), 400);
            }

            $data = [
                'name' => $request->name,
                'uri'   => $request->uri,
                'parent_id' => $request->parent_id
            ];

            if ($id) {
                $menu = Menu::find($id);
                $menu->update($data);
            } else {
                $menu = Menu::create($data);
            }

            return $this->success('success creating menu', 201, $menu);

        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }

    public function show($id)
    {
        try {

            $menu = Menu::find($id);

            if(!$menu) return $this->failure('Menu not found!');

            return $this->success('success retrieving data', 200, $menu);

        } catch (\Exception $e) {

            return $this->failure($e->getMessage());

        }
    }

    public function destroy($id)
    {
        try {

            DB::beginTransaction();

            $menu = Menu::with('subMenu')->find($id);

            if(!$menu) return $this->failure('Menu not found!');

            if ($menu->subMenu) {
                $menu->subMenu()->delete();
            }

            $menu->delete();

            DB::commit();

            return $this->success('success deleting menu', 200, $menu);

        } catch (\Exception $e) {

            DB::rollBack();
            return $this->failure($e->getMessage());

        }
    }

    public function getParentMenu()
    {
        try {

            $parentMenu = Menu::with('subMenu')->whereNull('parent_id')->get();
            return $this->success('success retrieving data', 200, $parentMenu);

        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }
}
