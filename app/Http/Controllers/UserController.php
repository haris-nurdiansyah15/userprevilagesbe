<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */

    public function index()
    {
        try {
            $users = User::with('accesses')->get();
            return $this->success('success retrieving data', 200, $users);
        } catch (\Exception $e) {
            return $this->failure($e->getMessage());
        }
    }

    public function store(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'username' => 'required|unique:users,username|max:255',
                'password' => 'required|max:255',
                "access_menus" => 'required|array'
            ]);

            if ($validator->fails()) {
                return $this->failure($validator->errors()->first(), 400);
            }

            DB::beginTransaction();

            $user = User::create([
                'username' => $request->username,
                'password' => Hash::make($request->password),
            ]);

            $user->accesses()->attach($request->access_menus);

            DB::commit();

            return $this->success('success creating superuser', 201, $user);

        } catch (\Exception $e) {

            DB::rollBack();
            return $this->failure($e->getMessage());

        }

    }

    public function update(Request $request, $id)
    {

        try {

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|unique:users,username,' . $id,
                "access_menus" => 'required|array'
            ]);

            if ($validator->fails()) {
                return $this->failure($validator->errors()->first(), 400);
            }

            DB::beginTransaction();

            $user = User::with('accesses')->find($id);

            if (!$user) return $this->failure('User not found!', 400);

            $user->username = $request->username;
            $request->password ? $user->password = Hash::make($request->password) : false;

            $user->save();

            $user->accesses()->sync($request->access_menus);

            DB::commit();

            return $this->success('success updating superuser', 201, $user);

        } catch (\Exception $e) {

            DB::rollBack();
            return $this->failure($e->getMessage());

        }

    }

    public function show($id)
    {
        try {

            $user = User::with('accesses')->find($id);

            if (!$user) {
                return $this->failure('User not found!', 400);
            }

            return $this->success('success retrieving data', 200, $user);

        } catch (\Exception $e) {

            return $this->failure($e->getMessage());

        }
    }

    public function destroy($id)
    {
        try {

            DB::beginTransaction();

            $user = User::find($id);

            if(!$user) return $this->failure('User not found!');

            $user->accesses()->detach();
            $user->delete();

            DB::commit();

            return $this->success('success deleting user', 200, $user);

        } catch (\Exception $e) {

            DB::rollBack();
            return $this->failure($e->getMessage());

        }
    }
}

// []
