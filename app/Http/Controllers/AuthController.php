<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->failure($validator->errors()->first(), 400);
        }

        $credentials = $request->only(['username', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return $this->failure('Unautorized', 400);
        }

        return $this->success('login successfully', 200, $this->respondWithToken($token));
    }

     /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'access_menu' => $this->user_access_menu(auth()->user()->id)
            // 'expires_in' => auth()->factory()->getTTL() * 60 * 24
        ];
    }

    protected function user_access_menu($user_id)
    {
        return Menu::whereHas('subMenu', function ($submenu) use ($user_id) {
            $submenu->whereHas('users', function ($user) use ($user_id) {
                $user->where('users.id', $user_id);
            });
        })
        ->with(['subMenu' => function ($with) use ($user_id) {
            $with->whereHas('users', function ($user) use ($user_id) {
                $user->where('users.id', $user_id);
            });
        }])
        ->whereNull('menus.parent_id')
        ->get();
    }
}

// []
