<?php

namespace App\Http\Controllers;

use App\Traits\RespondsWithHttpStatus;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use RespondsWithHttpStatus;
}
