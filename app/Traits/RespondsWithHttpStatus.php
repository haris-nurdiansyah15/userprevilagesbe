<?php

namespace App\Traits;

trait RespondsWithHttpStatus
{
    protected function success($message = 'success retrieving data', $code = 200, $data = null)
    {
        return response([
            'code' => $code,
            'error' => false,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    protected function failure($message = 'Internal server error', $code = 500)
    {
        return response([
            'code' => $code,
            'error' => true,
            'message' => $message,
        ], $code);
    }
}
