<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = [];
    protected $parentColumn = 'parent_id';

    public function subMenu()
    {
        return $this->hasMany(Self::class, $this->parentColumn);
    }

    public function parentMenu()
    {
        return $this->belongsTo(Self::class, $this->parentColumn);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_menu', 'menu_id', 'user_id');
    }
}
